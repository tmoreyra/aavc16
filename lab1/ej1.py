import numpy as np
lfrom bokeh.plotting import figure, show, output_file
from sklearn.svm import LinearSVC
from collections import defaultdict

from utils import (
    DATASET_PATH,
    CACHE_PATH,
    compute_features,
    compute_all_bovw,
    k_fold,
    load_scene_categories,
    n_per_class_split,
    setup_data,
)

# Learning configuration
N_CLUSTERS = 100
NORM_FEAT = 'l1'
NORM_BOVW = 'l2'

# Cross validation config
C_values = [10**-e for e in range(-5, 5)]
FOLDS = 5


def main():
    random_state = np.random.RandomState(1306)
    dataset = load_scene_categories(DATASET_PATH)

    compute_features(dataset['fname'], NORM_FEAT, CACHE_PATH)
    compute_all_bovw(dataset, N_CLUSTERS, NORM_BOVW, NORM_FEAT, random_state)

    train_set, test_set = n_per_class_split(dataset, 100, random_state)
    X_train, y_train = setup_data(train_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)
    
    results = defaultdict(list)
    for C in C_values:
        for i, (train_ix, val_ix) in enumerate(k_fold(len(train_set), FOLDS, random_state)):
            print('[C={}] Training fold {}'.format(C, i))
            X_train_fold = X_train[train_ix]
            y_train_fold = y_train[train_ix]
            X_val = X_train[val_ix]
            y_val = y_train[val_ix]

            svm = LinearSVC(C=C)
            svm.fit(X_train_fold, y_train_fold)
            y_pred = svm.predict(X_val)

            results[C].append((y_val == y_pred).mean())

        results[C] = np.array(results[C])

    bestC = max(results, key=lambda C: results[C].mean())
    print('Best accuracy ({}) found at C={}'.format(results[bestC].mean(), bestC))
    # Best accuracy (0.6333333333333334) found at C=10

    ## Plot

    fig = figure(
        tools="pan,wheel_zoom,reset,previewsave",
        x_axis_type="log",
        y_range=[0, 1], x_range=[10**-6, 10**6],
        title="Cross-validation para valores de C"
    )

    # K-fold points
    x = []
    y = []
    for c, accs in results.items():
        x.extend([c]*len(accs))
        y.extend(accs)

    fig.circle(x, y, size=4, alpha=0.3, color='navy')

    # Accuracy and std-dev for each fold (as error bars)
    err_x = []
    err_y = []
    ax = []
    ay = []
    for c, accs in results.items():
        m = accs.mean()
        s = accs.std()
        err_x.append([c, c])
        err_y.append([m-s, m+s])
        ax.append(c)
        ay.append(m)

    fig.multi_line(err_x, err_y, line_color='firebrick')
    fig.circle(ax, ay, size=7, color='firebrick')

    output_file('ej1.html')
    show(fig)

    # Results over test sets.
    print('Final testing with C =', bestC)

    X_train, y_train = setup_data(train_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)
    X_test, y_test = setup_data(test_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)
    svm = LinearSVC(C=bestC).fit(X_train, y_train)
    y_pred = svm.predict(X_test)
    results_test = [(y_pred == y_test).mean()]

    for _ in range(10):
        train_set, test_set = n_per_class_split(dataset, 100, random_state)
        X_train, y_train = setup_data(train_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)
        X_test, y_test = setup_data(test_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)
        svm = LinearSVC(C=bestC).fit(X_train, y_train)
        y_pred = svm.predict(X_test)
        results_test.append((y_pred == y_test).mean())

    results_test = np.array(results_test)
    print('Accuracy: {}, Std.: {}'.format(results_test.mean(), results_test.std()))
    # Accuracy: 0.6571950662402923, Std.: 0.004363513248319734


if __name__ == '__main__':
    main()
