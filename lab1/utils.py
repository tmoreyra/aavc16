from __future__ import print_function
from __future__ import division

import sys
from os import listdir, makedirs
from os.path import join, splitext, abspath, split, exists

import cv2
import numpy as np
from scipy.spatial import distance
from scipy.io import savemat, loadmat
from skimage.feature import daisy as skdaisy
from skimage.transform import rescale
from sklearn.svm import LinearSVC


np.seterr(all='raise')


BASE_PATH = abspath('scene_categories')
OUT_PATH = 'cache'

DATASET_PATH = BASE_PATH
CACHE_PATH = OUT_PATH


def setup_data(dataset, n_clusters, bovw_norm, feat_norm, encoding='bovw'):
    assert encoding in ('bovw', 'vlad')
    if encoding == 'bovw':
        get_file = get_bovwfile
    else:
        get_file = get_vladfile

    X, y = [], []
    for fname, cid in dataset:
        bovwfile = get_file(CACHE_PATH, fname, n_clusters, bovw_norm, feat_norm)
        X.append(load_data(bovwfile))
        y.append(cid)
    return np.array(X), np.array(y)


def get_bovwfile(path, fname, n_clusters, bovw_norm, feat_norm):
    ext = '.{}.{}.{}.bovw'.format(n_clusters, bovw_norm, feat_norm)
    return join(path, splitext(fname)[0] + ext)


def get_vladfile(path, fname, n_clusters, vlad_norm, feat_norm):
    ext = '.{}.{}.{}.vlad'.format(n_clusters, vlad_norm, feat_norm)
    return join(path, splitext(fname)[0] + ext)


def compute_all_bovw(dataset, n_clusters, norm, feat_norm, random_state):
    """
    Pre-compute all bovw for a given n_clusters and normalization
    """
    n_samples = int(1e5)

    # Compute vocabulary

    vocabulary_file = join(CACHE_PATH, 'vocabulary{:d}.{}.dat'.format(n_clusters, feat_norm))
    if exists(vocabulary_file):
        vocabulary = load_data(vocabulary_file)

    else:
        sample = sample_feature_set(dataset['fname'], OUT_PATH, n_samples, feat_norm,
                                    random_state=random_state)
        vocabulary = kmeans_fit(sample, n_clusters, random_state=random_state)
        save_data(vocabulary, vocabulary_file)

    print('{}: {} clusters'.format(vocabulary_file, vocabulary.shape[0]))

    # Compute BoVW vectors

    for fname in dataset['fname']:
        featfile = get_featfile(CACHE_PATH, fname, feat_norm)
        bovwfile = get_bovwfile(CACHE_PATH, fname, n_clusters, norm, feat_norm)

        if exists(bovwfile):
            print('{} already exists'.format(bovwfile))
            continue

        feat = load_data(featfile)
        bovw = compute_bovw(vocabulary, feat, norm)
        save_data(bovw, bovwfile)
        print('created', bovwfile)


def compute_all_vlad(dataset, n_clusters, norm, feat_norm, random_state):
    """
    Pre-compute all bovw for a given n_clusters and normalization
    """
    n_samples = int(1e5)

    # Compute vocabulary

    vocabulary_file = join(CACHE_PATH, 'vocabulary{:d}.{}.dat'.format(n_clusters, feat_norm))
    if exists(vocabulary_file):
        vocabulary = load_data(vocabulary_file)

    else:
        sample = sample_feature_set(dataset['fname'], OUT_PATH, n_samples, feat_norm,
                                    random_state=random_state)
        vocabulary = kmeans_fit(sample, n_clusters, random_state=random_state)
        save_data(vocabulary, vocabulary_file)

    print('{}: {} clusters'.format(vocabulary_file, vocabulary.shape[0]))

    # Compute BoVW vectors

    for fname in dataset['fname']:
        featfile = get_featfile(CACHE_PATH, fname, feat_norm)
        vladfile = get_vladfile(CACHE_PATH, fname, n_clusters, norm, feat_norm)

        if exists(vladfile):
            print('{} already exists'.format(vladfile))
            continue

        feat = load_data(featfile)
        vlad = compute_vlad(vocabulary, feat, norm)
        save_data(vlad, vladfile)
        print('created', vladfile)

 
def norm_L1(v):
    norm = np.linalg.norm(v, ord=1)
    return v / (norm + 1e-7)


def norm_L2(v):
    norm = np.linalg.norm(v, ord=2)
    return v / (norm + 1e-7)


def norm_sqrt(v):
    norm = np.sum([np.sign(x) * np.sqrt(np.abs(x)) for x in v])
    return v / (norm + 1e-7)


NORMS = {
    'off': lambda v: v,
    'l1': norm_L1,
    'l2': norm_L2,
    'sqrt': norm_sqrt,
    'sqrt+l2': lambda v: norm_sqrt(norm_L1(v))  # Cheaper than norm_L2(norm_sqrt(v))
}


def k_fold(size, K, random_state=None):
    if random_state is None:
        random_state = np.random.RandomState()
    ixs = np.arange(size)
    random_state.shuffle(ixs)
    folds = np.array(np.array_split(ixs, K))
    for j in range(K):
        yield np.concatenate(folds[[i for i in range(K) if i != j]]), folds[j]


def daisy(*args, normalization=None, **kwargs):
    assert normalization in ('l1', 'l2', 'sqrt', 'l1+sqrt', 'sqrt+l2', 'off')

    if normalization in ('l1', 'l2', 'off'):
        return skdaisy(*args, normalization=normalization, **kwargs)

    if normalization == 'sqrt':
        return norm_sqrt(skdaisy(*args, normalization='off', **kwargs))

    if normalization in ('sqrt+l2', 'l1+sqrt'):
        # are equivalent
        return norm_sqrt(skdaisy(*args, normalization='l1', **kwargs))


def save_data(data, filename, force_overwrite=False):
    # if dir/subdir doesn't exist, create it
    dirname = split(filename)[0]
    if not exists(dirname):
        makedirs(dirname)
    savemat(filename, {'data': data}, appendmat=False)


def load_data(filename):
    return loadmat(filename, appendmat=False)['data'].squeeze()


def load_scene_categories(path, random_state=None):
    cname = sorted(listdir(path))  # human-readable class names
    cid = []  # class id wrt cname list
    fname = []  # relative file paths
    for i, cls in enumerate(cname):
        for img in listdir(join(path, cls)):
            if splitext(img)[1] not in ('.jpeg', '.jpg', '.png'):
                continue
            fname.append(join(cls, img))
            cid.append(i)
    return {'cname': cname, 'cid': cid, 'fname': fname}


def n_per_class_split(dataset, n=100, random_state=None):
    """
    Split dataset into train-test sets.
    The test set is made of 'n' random elements of each class.
    """
    # set RNG
    if random_state is None:
        random_state = np.random.RandomState()

    n_classes = len(dataset['cname'])
    cid = dataset['cid']
    fname = dataset['fname']

    train_set = []
    test_set = []
    for id_ in range(n_classes):
        idxs = [i for i, j in enumerate(cid) if j == id_]
        random_state.shuffle(idxs)

        # train samples
        for i in idxs[:n]:
            train_set.append((fname[i], cid[i]))

        # test samples
        for i in idxs[n:]:
            test_set.append((fname[i], cid[i]))

    random_state.shuffle(train_set)
    random_state.shuffle(test_set)
    return train_set, test_set


SCALES_3 = [1.0, 0.5, 0.25]
SCALES_5 = [1.0, 0.707, 0.5, 0.354, 0.25]
def extract_multiscale_dense_features(imfile, norm, step=8, scales=SCALES_3):
    """
    """
    im = cv2.imread(imfile, cv2.IMREAD_GRAYSCALE)
    feat_all = []
    for sc in scales:
        dsize = (int(sc * im.shape[0]), int(sc * im.shape[1]))
        im_scaled = cv2.resize(im, dsize, interpolation=cv2.INTER_LINEAR)
        feat = daisy(im_scaled, normalization=norm, step=step)
        if feat.size == 0:
            break
        ndim = feat.shape[2]
        feat = np.atleast_2d(feat.reshape(-1, ndim))
        feat_all.append(feat)
    return np.row_stack(feat_all).astype(np.float32)


def compute_features(im_list, norm, output_path):
    """
    compute and store low level features for all images
    """
    for fname in im_list:
        imfile = join(BASE_PATH, fname)
        featfile = get_featfile(output_path, fname, norm)

        if exists(featfile):
            print('{} already exists'.format(featfile))
            continue

        feat = extract_multiscale_dense_features(imfile, norm)
        save_data(feat, featfile)
        print('{}: {} features'.format(featfile, feat.shape[0]))


def get_featfile(path, fname, norm):
    return join(path, splitext(fname)[0] + '.{}.feat'.format(norm))


def sample_feature_set(im_list, output_path, n_samples, norm, random_state=None):
    BASE_PATH = output_path
    if random_state is None:
        random_state = np.random.RandomState()

    sample_file = join(output_path, 'sample{:d}.{}.feat'.format(n_samples, norm))
    if exists(sample_file):
        return load_data(sample_file)

    sample = []
    while len(sample) < n_samples:
        i = random_state.randint(0, len(im_list))
        featfile = get_featfile(CACHE_PATH, im_list[i], norm)
        feat = load_data(featfile)
        idxs = random_state.choice(range(feat.shape[0]), 100)  # 100 per file
        sample += [feat[i] for i in idxs]
        # print('\r{}/{} samples'.format(len(sample), n_samples), end='')
        sys.stdout.flush()

    sample = np.row_stack(sample)
    save_data(sample, sample_file)

    print('\r{}: {} features'.format(sample_file, sample.shape[0]))
    return sample


def kmeans_fit(samples, n_clusters, maxiter=100, tol=1e-4, random_state=None):
    """
    Compute ´n_clusters´ centroids for the given samples.
    """ 
    if random_state is None:
        random_state = np.random.RandomState()

    n_samples = samples.shape[0]    

    # better inicialization to avoid "k-means crash"
    centroids = []
    for i in range(n_clusters):
        idxs = random_state.randint(0, n_samples, 5)
        cs = samples[idxs, :]
        centroids.append([np.mean(xs) for xs in zip(*cs)])
    centroids = np.array(centroids)

    # chose random samples as initial estimates
    # idxs = random_state.randint(0, n_samples, n_clusters)
    # centroids = samples[idxs, :]

    J_old = np.inf
    for iter_ in range(maxiter):

        # SAMPLE-TO-CLUSTER ASSIGNMENT

        # cdist returns a matrix of size n_samples x n_clusters, where the i-th
        # row stores the (squared) distance from sample i to all centroids
        dist2 = distance.cdist(samples, centroids, metric='sqeuclidean')

        # argmin over columns of the distance matrix
        assignment = np.argmin(dist2, axis=1)

        # CENTROIDS UPDATE (+ EVAL DISTORTION)

        J_new = 0.
        for k in range(n_clusters):
            idxs = np.where(assignment == k)[0]
            if len(idxs) == 0:
                raise RuntimeError('k-means crash!')

            centroids[k, :] = np.mean(samples[idxs], axis=0).astype(np.float32)

            J_new += np.sum(dist2[idxs, assignment[idxs]])
        J_new /= float(n_samples)

        print('iteration {}, potential={:.3e}'.format(iter_, J_new))
        if (J_old - J_new) / J_new < tol:
            print('STOP')
            break
        J_old = J_new

    return centroids


def compute_bovw(vocabulary, features, norm):
    """
    Given the visual vocabulary and the features, compute the
    normalized histogram of ocurrences.
    """
    if vocabulary.shape[1] != features.shape[1]:
        raise RuntimeError('something is wrong with the data dimensionality')
    dist2 = distance.cdist(features, vocabulary, metric='sqeuclidean')
    assignments = np.argmin(dist2, axis=1)
    bovw, _ = np.histogram(assignments, range(vocabulary.shape[1]))
    return NORMS[norm](bovw)


def compute_vlad(vocabulary, features, norm):
    if vocabulary.shape[1] != features.shape[1]:
        raise RuntimeError('something is wrong with the data dimensionality')
    dist2 = distance.cdist(features, vocabulary, metric='sqeuclidean')
    assignments = np.argmin(dist2, axis=1)

    all_vlad = []
    for centroid_ix, feature in zip(assignments, features):
        vlad = np.zeros([vocabulary.shape[0], vocabulary.shape[1]])  # FIXME vocabulary.shape
        vlad[centroid_ix] = np.abs(feature - vocabulary[centroid_ix])
        all_vlad.append(np.hstack(vlad))

    final_vlad = np.sum(np.row_stack(all_vlad), axis=0)
    return NORMS[norm](final_vlad)
