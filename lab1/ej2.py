import numpy as np
from sklearn.svm import LinearSVC
from collections import defaultdict
from bokeh.plotting import figure, show, output_file

from utils import (
    DATASET_PATH,
    CACHE_PATH,
    compute_features,
    compute_all_bovw,
    k_fold,
    load_scene_categories,
    n_per_class_split,
    setup_data,
)

# Learning configuration
NORM_FEAT = 'l1'
NORM_BOVW = 'l2'
C = 10

# Cross validation config
n_clusters_values = [50, 100, 150, 200, 250, 300]
FOLDS = 5


def main():
    random_state = np.random.RandomState(1306)
    dataset = load_scene_categories(DATASET_PATH)
    compute_features(dataset['fname'], NORM_FEAT, CACHE_PATH)
    train_set, test_set = n_per_class_split(dataset, 100, random_state)

    results = defaultdict(list)
    for n_clusters in n_clusters_values:
        compute_all_bovw(dataset, n_clusters, NORM_BOVW, NORM_FEAT, random_state)
        X_train, y_train = setup_data(train_set, n_clusters, NORM_BOVW, NORM_FEAT)

        for i, (train_ix, val_ix) in enumerate(k_fold(len(train_set), FOLDS, random_state)):
            print('[n_clusters={}] Training fold {}'.format(n_clusters, i))
            X_train_fold = X_train[train_ix]
            y_train_fold = y_train[train_ix]
            X_val = X_train[val_ix]
            y_val = y_train[val_ix]

            svm = LinearSVC(C=C)
            svm.fit(X_train_fold, y_train_fold)
            y_pred = svm.predict(X_val)

            results[n_clusters].append((y_val == y_pred).mean())

        results[n_clusters] = np.array(results[n_clusters])

    best = max(results, key=lambda n: results[n].mean())
    print('Best accuracy ({}) found with n_clusters={}'.format(results[best].mean(), best))

    ## Plot

    fig = figure(
        tools="pan,wheel_zoom,reset,previewsave",
        title="Cross-validation para diferentes números de clusters"
    )

    # K-fold points
    x = []
    y = []
    for n, accs in results.items():
        x.extend([n]*len(accs))
        y.extend(accs)

    fig.circle(x, y, size=4, alpha=0.3, color='navy')

    # Accuracy and std-dev for each fold (as error bars)
    err_x = []
    err_y = []
    ax = []
    ay = []
    for n, accs in results.items():
        m = accs.mean()
        s = accs.std()
        err_x.append([n, n])
        err_y.append([m-s, m+s])
        ax.append(n)
        ay.append(m)

    fig.multi_line(err_x, err_y, line_color='firebrick')
    fig.circle(ax, ay, size=7, color='firebrick')

    output_file('ej2.html')
    show(fig)

    # Results over test sets.
    print('Final testing with num_clusters =', best)
    # Best accuracy (0.6286666666666666) found with n_clusters=100

    X_train, y_train = setup_data(train_set, best, NORM_BOVW, NORM_FEAT)
    X_test, y_test = setup_data(test_set, best, NORM_BOVW, NORM_FEAT)
    svm = LinearSVC(C=C).fit(X_train, y_train)
    y_pred = svm.predict(X_test)
    results_test = [(y_pred == y_test).mean()]

    for _ in range(10):
        train_set, test_set = n_per_class_split(dataset, 100, random_state)
        X_train, y_train = setup_data(train_set, best, NORM_BOVW, NORM_FEAT)
        X_test, y_test = setup_data(test_set, best, NORM_BOVW, NORM_FEAT)
        svm = LinearSVC(C=C).fit(X_train, y_train)
        y_pred = svm.predict(X_test)
        results_test.append((y_pred == y_test).mean())

    results_test = np.array(results_test)
    print('Accuracy: {}, Std.: {}'.format(results_test.mean(), results_test.std()))
    # Accuracy: 0.6536927059540125, Std.: 0.006269208453090321

    
if __name__ == '__main__':
    main()
