import numpy as np
from sklearn.svm import LinearSVC
from collections import defaultdict
from itertools import product
from bokeh.plotting import figure, show, output_file

from utils import (
    DATASET_PATH,
    CACHE_PATH,
    compute_features,
    compute_all_bovw,
    k_fold,
    load_scene_categories,
    n_per_class_split,
    setup_data,
)

# Learning configuration
C = 10
N_CLUSTERS = 100

# Cross validation config
NORM_FEAT_values = ['off', 'l2', 'sqrt'] #, 'sqrt+l2']
NORM_BOVW_values = ['off', 'l2', 'sqrt', 'sqrt+l2']
FOLDS = 5


def main():
    random_state = np.random.RandomState(1306)
    dataset = load_scene_categories(DATASET_PATH)
    train_set, test_set = n_per_class_split(dataset, 100, random_state)

    results = defaultdict(lambda: defaultdict(list))
    for NORM_FEAT, NORM_BOVW in product(NORM_FEAT_values, NORM_BOVW_values):
        compute_features(dataset['fname'], NORM_FEAT, CACHE_PATH)
        compute_all_bovw(dataset, N_CLUSTERS, NORM_BOVW, NORM_FEAT, random_state)
        X_train, y_train = setup_data(train_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)

        for i, (train_ix, val_ix) in enumerate(k_fold(len(train_set), FOLDS, random_state)):
            print('Norms[feat={}, bavw={}] Training fold {}'.format(NORM_FEAT, NORM_BOVW, i))
            X_train_fold = X_train[train_ix]
            y_train_fold = y_train[train_ix]
            X_val = X_train[val_ix]
            y_val = y_train[val_ix]

            svm = LinearSVC(C=C)
            svm.fit(X_train_fold, y_train_fold)
            y_pred = svm.predict(X_val)

            results[NORM_FEAT][NORM_BOVW].append((y_val == y_pred).mean())

        results[NORM_FEAT][NORM_BOVW] = np.array(results[NORM_FEAT][NORM_BOVW])

    # Find best pair
    acc = np.float('-inf')
    values = list(product(NORM_FEAT_values, NORM_BOVW_values))
    for nf, nb in values:
        _acc = results[nf][nb].mean()
        if _acc > acc:
            acc = _acc
            norm_feat, norm_bovw = nf, nb

    print('Best accuracy ({}) found with norm_feat={}, norm_bovw={}'.format(acc, norm_feat, norm_bovw))

    ## Plot

    fig = figure(
        tools="pan,wheel_zoom,reset,previewsave",
        title="Cross-validation para diferentes transformaciones"
    )

    ts = {'off': 0, 'sqrt': 1, 'l2': 2, 'sqrt+l2': 3}  # plot convenience

    # K-fold points
    x = []
    y = []
    for nf, nb in values:
        # El calculo del tamanio es para que sea coherente con el accuracy, aunque no
        # sea proporcional - pero que sean visibles las diferencias.
        fig.circle([ts[nf]], [ts[nb]], size=2**(results[nf][nb]*10)/3, color='navy', alpha=0.8)

    output_file('ej3.html')
    show(fig)

    # Results over test sets.
    print('Final testing with norm_feat, norm_bovw =', norm_feat, norm_bovw)

    X_train, y_train = setup_data(train_set, N_CLUSTERS, norm_bovw, norm_feat)
    X_test, y_test = setup_data(test_set, N_CLUSTERS, norm_bovw, norm_feat)
    svm = LinearSVC(C=C).fit(X_train, y_train)
    y_pred = svm.predict(X_test)
    results_test = [(y_pred == y_test).mean()]

    for _ in range(10):
        train_set, test_set = n_per_class_split(dataset, 100, random_state)
        X_train, y_train = setup_data(train_set, N_CLUSTERS, norm_bovw, norm_feat)
        X_test, y_test = setup_data(test_set, N_CLUSTERS, norm_bovw, norm_feat)
        svm = LinearSVC(C=C).fit(X_train, y_train)
        y_pred = svm.predict(X_test)
        results_test.append((y_pred == y_test).mean())

    results_test = np.array(results_test)
    print('Accuracy: {}, Std.: {}'.format(results_test.mean(), results_test.std()))
    # Accuracy: 0.6536927059540125, Std.: 0.006269208453090321


if __name__ == '__main__':
    main()
