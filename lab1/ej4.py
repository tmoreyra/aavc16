import numpy as np
from sklearn.svm import SVC
from collections import defaultdict
from itertools import product
from bokeh.plotting import figure, show, output_file

from utils import (
    DATASET_PATH,
    CACHE_PATH,
    compute_features,
    compute_all_bovw,
    k_fold,
    load_scene_categories,
    n_per_class_split,
    setup_data,
)

# Learning configuration
N_CLUSTERS = 100
NORM_FEAT = 'l1'
NORM_BOVW = 'l2'
KERNEL = 'rbf'

# Cross validation config
GAMMA_values = [10**-e for e in range(-6, 3)]
C_values = [10**-e for e in range(-2, 7)]
FOLDS = 5


def main():
    random_state = np.random.RandomState(1306)
    dataset = load_scene_categories(DATASET_PATH)
    train_set, test_set = n_per_class_split(dataset, 100, random_state)
    compute_features(dataset['fname'], NORM_FEAT, CACHE_PATH)
    compute_all_bovw(dataset, N_CLUSTERS, NORM_BOVW, NORM_FEAT, random_state)
    X_train, y_train = setup_data(train_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)

    cv_values = list(product(C_values, GAMMA_values))
    results = defaultdict(lambda: defaultdict(list))
    for C, gamma in cv_values:
        for i, (train_ix, val_ix) in enumerate(k_fold(len(train_set), FOLDS, random_state)):
            print('[C={}, gamma={}] Training fold {}'.format(C, gamma, i))
            X_train_fold = X_train[train_ix]
            y_train_fold = y_train[train_ix]
            X_val = X_train[val_ix]
            y_val = y_train[val_ix]

            svm = SVC(C=C, kernel=KERNEL, gamma=gamma)
            svm.fit(X_train_fold, y_train_fold)
            y_pred = svm.predict(X_val)

            results[C][gamma].append((y_val == y_pred).mean())

        results[C][gamma] = np.array(results[C][gamma])

    # Find best pair
    acc = np.float('-inf')
    for c, g in cv_values:
        _acc = results[c][g].mean()
        if _acc > acc:
            acc = _acc
            C, gamma = c, g

    print('Best accuracy ({}) found with C={}, gamma={}'.format(acc, C, gamma))

    ## Plot

    fig = figure(
        tools="pan,wheel_zoom,reset,previewsave",
        title="Cross-validation para los parametros C y gamma",
        x_axis_type='log', y_axis_type='log',
        x_range=(10**-5, 10**5), y_range=(10**-5, 10**5)
    )

    # K-fold points
    for c, g in cv_values:
        # El calculo del tamanio es para que sea coherente con el accuracy, aunque no
        # sea proporcional - pero que sean visibles las diferencias.
        fig.circle(c, g, size=2**(results[c][g]*10)/3, color='navy', alpha=0.8)

    output_file('ej4-rbf.html')
    show(fig)

    # Results over test sets.
    print('Final testing with C, gamma =', C, gamma)

    X_train, y_train = setup_data(train_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)
    X_test, y_test = setup_data(test_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)
    svm = SVC(C=C, kernel=KERNEL, gamma=gamma).fit(X_train, y_train)
    y_pred = svm.predict(X_test)
    results_test = [(y_pred == y_test).mean()]

    for _ in range(10):
        train_set, test_set = n_per_class_split(dataset, 100, random_state)
        X_train, y_train = setup_data(train_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)
        X_test, y_test = setup_data(test_set, N_CLUSTERS, NORM_BOVW, NORM_FEAT)
        svm = SVC(C=C, kernel=KERNEL, gamma=gamma).fit(X_train, y_train)
        y_pred = svm.predict(X_test)
        results_test.append((y_pred == y_test).mean())

    results_test = np.array(results_test)
    print('Accuracy: {}, Std.: {}'.format(results_test.mean(), results_test.std()))


if __name__ == '__main__':
    main()
